package aplicacion;
import dominio.*;

public class Principal{
	public static void main(String[] args){
		
		String[] nombre_municipios = {"Alcorcon", "alcobendas"};
		String[] nombre_localidades = {"Alcorcon centro", "Alcorcon estacion", "Alcorcon Mcdonalds","Alcobendas centro", "Alcobendas estacion", "Alcobendas Mcdonalds"};
		int[] habitantes_localidades = {1000, 2000, 1500, 1250, 1500, 2300};

		Provincia provincia = new Provincia();
		provincia.setNombre("Madrid");
		//bucle que recorre desde i=o hasta todo los municipios
		for (int i = 0; i < nombre_municipios.length; i++){
			Municipio mun = new Municipio();
			mun.setNombre(nombre_municipios[i]);
			/*
			* dentro de cada municipio crea una localidad, variables distintas!
			* la primera vez la i vale 0 y devuelve 3, la siguiente...
			* la primera vez quiero que empiece en 0 pero no siempre
			* si no quiero que empiece en 0, j=i*3 (valor inical de la j)
			*/
			for (int j = 0; j < (i+1)*3;j++){
				Localidad loc = new Localidad();
				loc.setNombre(nombre_localidades[j]);
				loc.setNumeroDeHabitantes(habitantes_localidades[j]);
				mun.addLocalidad(loc);
		}
			provincia.addMunicipio(mun);
		}
		System.out.println(provincia);
		


	}
}
